﻿using System;
using System.Collections.Generic;
using System.Data.SqlServerCe;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using ASEAc3357404.Dtos;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ASEAc3357404.Tests
{
    [TestClass]
    public class MethodsTests
    {
        SqlCeConnection _connection;

        public MethodsTests()
        {
            _connection = new SqlCeConnection(@"Data Source=I:\ASEAc3357404\ASEAdb.sdf");
        }

        [TestMethod]
        public void GetAllProjects_ReturnsListOfDtos()
        {
            _connection.Open();
            var methods = new Methods(_connection);

            var projects = methods.GetAllProjects();

            Assert.IsTrue(projects != null, "GetAllProjects returned null!");
            _connection.Dispose();
        }

        [TestMethod]
        public void CreateNewProject_CreatesNewProject()
        {
            // Use a scope to not actually write to the db
            using (var scope = new TransactionScope(TransactionScopeOption.Required))
            {
                // The actual Dto that will be returned. We will write to this
                ProjectDto actualDto = new ProjectDto();
                // Open the connection here because if we pass it in the constructor, we will get Connection already Open errors
                _connection.Open();
                var methods = new Methods(_connection);

                // Call the method we are testing
                methods.CreateNewProject("test 123", "sam");

                // Read from the db to see if we have written to the db
                SqlCeDataReader dataReader = new SqlCeCommand("SELECT * FROM Projects WHERE ProjectName = 'test 123'", _connection).ExecuteReader();

                // Create a new dto which will contain the data we are expecting
                ProjectDto expectedDto = new ProjectDto()
                {
                    ProjectId = 0,
                    ProjectName = "test 123",
                    Author = "sam"
                };

                // Read the from the db
                while (dataReader.Read())
                {
                    actualDto.ProjectName = (string)dataReader["ProjectName"];
                    actualDto.Author = (string)dataReader["Author"];
                }

                // Assert that the dto we created that we are expecting is the same as what we got back from the db
                Assert.AreEqual(expectedDto.ProjectName, actualDto.ProjectName);
                Assert.AreEqual(expectedDto.Author, actualDto.Author);

                // End our Transaction Scope
                scope.Complete();
            }
        }

        [TestMethod]
        public void CreateNewBug_CreatesNewBug()
        {
            using (var scope = new TransactionScope(TransactionScopeOption.Required))
            {
                BugDto actualDto = new BugDto();
                _connection.Open();
                var methods = new Methods(_connection);

                methods.CreateNewBug("1", "test 123", "test123", (decimal)1, "tgth54ce", "Sam", "Sam");

                SqlCeDataReader dataReader = new SqlCeCommand("SELECT * FROM Bugs WHERE BugName = 'test 123'", _connection).ExecuteReader();

                BugDto expectedDto = new BugDto()
                {
                    BugId = 0,
                    BugName = "test 123",
                    ClassName = "test123",
                    LineNumber = 1,
                    RevisionNumber = "tgth54ce",
                    ReportingAuthor = "Sam",
                    CodeAuthor = "Sam"
                };

                while (dataReader.Read())
                {
                    actualDto.BugName = (string)dataReader["BugName"];
                    actualDto.ClassName = (string)dataReader["ClassName"];
                    actualDto.LineNumber = (int)dataReader["LineNumber"];
                    actualDto.RevisionNumber = (string)dataReader["RevisionNumber"];
                    actualDto.ReportingAuthor = (string)dataReader["ReportingAuthor"];
                    actualDto.CodeAuthor = (string)dataReader["CodeAuthor"];
                }

                Assert.AreEqual(expectedDto.BugName, actualDto.BugName);
                Assert.AreEqual(expectedDto.ClassName, actualDto.ClassName);
                Assert.AreEqual(expectedDto.LineNumber, actualDto.LineNumber);
                Assert.AreEqual(expectedDto.RevisionNumber, actualDto.RevisionNumber);
                Assert.AreEqual(expectedDto.ReportingAuthor, actualDto.ReportingAuthor);
                Assert.AreEqual(expectedDto.CodeAuthor, actualDto.CodeAuthor);
                scope.Complete();
            }
        }
    }
}
