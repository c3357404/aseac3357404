﻿using System;
using System.Collections.Generic;
using System.Data.SqlServerCe;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ASEAc3357404.Dtos;

namespace ASEAc3357404
{
    public class Methods
    {
        private SqlCeConnection _connection;

        /// <summary>
        /// The Constructor for the Methods class
        /// </summary>
        /// <param name="connection">The Connection String to be used by this Class</param>
        public Methods(SqlCeConnection connection)
        {
            _connection = connection;
        }

        /// <summary>
        /// This will get all the Projects from the Project table
        /// </summary>
        /// <returns>A List of Dtos containing the data from the fields included in the Projects Table</returns>
        public List<ProjectDto> GetAllProjects()
        {
            List<ProjectDto> projects = new List<ProjectDto>();

            try
            {
                string selectProjectsCommand = "SELECT ProjectId, ProjectName, Author FROM Projects ORDER BY ProjectName";
                SqlCeCommand getProjects = new SqlCeCommand(selectProjectsCommand, _connection);

                SqlCeDataReader dataReader = getProjects.ExecuteReader();

                while (dataReader.Read())
                {
                    var projectDto = new ProjectDto()
                    {
                        ProjectId = (int)dataReader["ProjectId"],
                        ProjectName = (string)dataReader["ProjectName"]
                    };

                    projects.Add(projectDto);
                }

                return projects;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// This will get all the Bugs for the given Project ID
        /// </summary>
        /// <param name="projectId">The Project ID you want the Bugs for</param>
        /// <returns>A List of Bug Dtos containing the data from the fields included in the Bugs Table</returns>
        public List<BugDto> GetBugsForProject(string projectId)
        {
            List<BugDto> bugs = new List<BugDto>();

            try
            {
                string selectBugsCommand = string.Format(
                "SELECT BugId, ProjectId, BugName, ClassName, LineNumber, RevisionNumber, ReportingAuthor FROM Bugs WHERE ProjectId = {0} AND Resolved = 0 ORDER BY BugId",
                projectId);
                 
                SqlCeCommand getBugs = new SqlCeCommand(selectBugsCommand, _connection);

                SqlCeDataReader dataReader = getBugs.ExecuteReader();

                while (dataReader.Read())
                {
                    var bugDto = new BugDto()
                    {
                        BugId = (int)dataReader["BugId"],
                        ProjectId = (int)dataReader["ProjectId"],
                        BugName = (string)dataReader["BugName"],
                        ClassName = (string)dataReader["ClassName"],
                        LineNumber = (int)dataReader["LineNumber"],
                        ReportingAuthor = (string)dataReader["ReportingAuthor"],
                        RevisionNumber = (string)dataReader["RevisionNumber"]
                    };

                    bugs.Add(bugDto);
                }

                return bugs;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// This will create a new project using the values passed through
        /// </summary>
        /// <param name="projectName">The name of the Project to be created</param>
        /// <param name="author">The Author of the Project to be created</param>
        public void CreateNewProject(string projectName, string author)
        {
            try
            {
                string createProjectCommand = string.Format("INSERT INTO Projects (ProjectName, Author) VALUES ('{0}', '{1}');", projectName, author);

                SqlCeCommand createProject = new SqlCeCommand(createProjectCommand, _connection);
                createProject.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// This will edit the existing project based on the values passed through
        /// </summary>
        /// <param name="projectId">The ID of the Project to be edited</param>
        /// <param name="projectName">The new Project Name</param>
        /// <param name="author">The new Author</param>
        public void EditProject(string projectId, string projectName, string author)
        {
            try
            {
                string editProjectCommand = string.Format("UPDATE Projects SET ProjectName = '{0}', Author = '{1}' WHERE ProjectId = '{2}';",
                    projectName, author, projectId);

                SqlCeCommand editProject = new SqlCeCommand(editProjectCommand, _connection);
                editProject.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// This will create a new bug using the values to be passed through
        /// </summary>
        /// <param name="projectId">The ID of the Project the Bug will belong to</param>
        /// <param name="bugName">The name of the Bug</param>
        /// <param name="className">The class the Bug was found in</param>
        /// <param name="lineNumber">The line where the Bug was found</param>
        /// <param name="revisionNumber">The revision the Bug was found in</param>
        /// <param name="codeAuthor">The author of the original, buggy code</param>
        /// <param name="reportingAuthor">The author of the person who reported the Bug</param>
        public void CreateNewBug(string projectId, string bugName, string className, decimal lineNumber, string revisionNumber, string codeAuthor, string reportingAuthor)
        {
            try
            {
                string createBugCommand =
                    string.Format(
                    "INSERT INTO Bugs (ProjectId, BugName, ClassName, LineNumber, RevisionNumber, CodeAuthor, ReportingAuthor, Resolved) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', 0);",
                    projectId,
                    bugName,
                    className,
                    lineNumber,
                    revisionNumber,
                    codeAuthor,
                    reportingAuthor);

                SqlCeCommand createBug = new SqlCeCommand(createBugCommand, _connection);
                createBug.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// This will get a Project Dto based on the value passed through
        /// </summary>
        /// <param name="projectId">The ID of the Project that is being requested</param>
        /// <returns>A single Dto containing the data from the fields included in the Projects Table</returns>
        public ProjectDto GetProject(string projectId)
        {
            try
            {
                ProjectDto project = new ProjectDto();
                string getProjectCommand = string.Format("SELECT * FROM Projects WHERE ProjectId = {0}", projectId);

                SqlCeCommand getProject = new SqlCeCommand(getProjectCommand, _connection);
                SqlCeDataReader dataReader = getProject.ExecuteReader();

                while (dataReader.Read())
                {
                    project.ProjectId = (int)dataReader["ProjectId"];
                    project.ProjectName = (string)dataReader["ProjectName"];
                    project.Author = (string)dataReader["Author"];
                }

                return project;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// This will get a Bug Dto based on the value passed through
        /// </summary>
        /// <param name="bugId">The ID of the Bug that is being requested</param>
        /// <returns>A Dto containing the data from the fields included in the Bugs Table</returns>
        public BugDto GetBug(string bugId)
        {
            try
            {
                BugDto bug = new BugDto();

                string getBugCommand = string.Format("SELECT * FROM Bugs WHERE BugId = {0}", bugId);

                SqlCeCommand getBug = new SqlCeCommand(getBugCommand, _connection);
                SqlCeDataReader dataReader = getBug.ExecuteReader();

                while (dataReader.Read())
                {
                    bug.BugId = (int)dataReader["BugId"];
                    bug.BugName = (string)dataReader["BugName"];
                    bug.ClassName = (string)dataReader["ClassName"];
                    bug.LineNumber = (int)dataReader["LineNumber"];
                    bug.RevisionNumber = (string)dataReader["RevisionNumber"];
                    bug.CodeAuthor = (string)dataReader["CodeAuthor"];
                    bug.ReportingAuthor = (string)dataReader["ReportingAuthor"];
                }

                return bug;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Marks a bug as resolved
        /// </summary>
        /// <param name="bugId">The ID of the Bug to be marked as resolved</param>
        public void MarkBugAsResolved(string bugId)
        {
            try
            {
                string markBugCommand = string.Format("UPDATE Bugs SET Resolved = '1' WHERE BugId = '{0}'", bugId);

                SqlCeCommand markBug = new SqlCeCommand(markBugCommand, _connection);
                markBug.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
