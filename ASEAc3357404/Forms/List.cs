﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe;
using ASEAc3357404.Dtos;
using System.Collections;

namespace ASEAc3357404
{
    public partial class List : Form
    {
        SqlCeConnection connection;
        public List()
        {
            InitializeComponent();
            connection = new SqlCeConnection(@"Data Source=I:\ASEAc3357404\ASEAdb.sdf");
            connection.Open();
            populateProjects();
            populateBugs();
        }

        /// <summary>
        /// This will populate the Projects List in the List Form
        /// </summary>
        public void populateProjects()
        {
            var projectDtos = new Methods(connection).GetAllProjects();

            List<KeyValuePair<int, string>> availableProjects = new List<KeyValuePair<int, string>>();
            foreach (var project in projectDtos)
            {
                availableProjects.Add(new KeyValuePair<int, string>(project.ProjectId, project.ProjectName));
            }
            projectList.DataSource = availableProjects;
            projectList.ValueMember = "Key";
            projectList.DisplayMember = "Value";
        }

        /// <summary>
        /// This will populate the Bugs List in the List Form
        /// </summary>
        public void populateBugs()
        {
            var selected = (KeyValuePair<int, string>)projectList.SelectedItem;
            string projectId = selected.Key.ToString();
            var bugDtos = new Methods(connection).GetBugsForProject(projectId);

            //var bugDtos = new List<BugDto>();

            List<KeyValuePair<int, string>> availableBugs = new List<KeyValuePair<int, string>>();
            foreach (var bug in bugDtos)
            {
                availableBugs.Add(new KeyValuePair<int, string>(bug.BugId, bug.BugName));
            }
            bugList.DataSource = availableBugs;
            bugList.ValueMember = "Key";
            bugList.DisplayMember = "Value";
        }

        private void newProject_Click(object sender, EventArgs e)
        {
            ProjectAdd projectAddForm = new ProjectAdd(this);

            projectAddForm.Show();
        }

        private void newBug_Click(object sender, EventArgs e)
        {
            var selectedProject = projectList.SelectedItem.ToString();

            var projectId = projectList.GetItemText(projectList.SelectedValue);
            var projectName = projectList.GetItemText(projectList.SelectedItem);

            BugAdd bugAddForm = new BugAdd(this, projectId, projectName);

            bugAddForm.Show();
        } 

        private void projectList_SelectedIndexChanged(object sender, EventArgs e)
        {
            populateBugs();
        }

        private void editButton_Click(object sender, EventArgs e)
        {
            var selectedProject = projectList.SelectedItem.ToString();

            var projectId = projectList.GetItemText(projectList.SelectedValue);

            ProjectEdit projectEdit = new ProjectEdit(this, projectId);
            projectEdit.Show();
        }

        private void viewButton_Click(object sender, EventArgs e)
        {
            var selectedProject = bugList.SelectedItem.ToString();

            var bugId = bugList.GetItemText(bugList.SelectedValue);

            BugView bugView = new BugView(this, bugId);
            bugView.Show();
        }
    }
}
