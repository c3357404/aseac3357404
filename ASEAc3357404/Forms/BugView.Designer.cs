﻿namespace ASEAc3357404
{
    partial class BugView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bugName = new System.Windows.Forms.Label();
            this.classNameLabel = new System.Windows.Forms.Label();
            this.lineNumberLabel = new System.Windows.Forms.Label();
            this.revisionNumberLabel = new System.Windows.Forms.Label();
            this.codeAuthorLabel = new System.Windows.Forms.Label();
            this.reportingAuthorLabel = new System.Windows.Forms.Label();
            this.className = new System.Windows.Forms.Label();
            this.lineNumber = new System.Windows.Forms.Label();
            this.revisionNumber = new System.Windows.Forms.Label();
            this.codeAuthor = new System.Windows.Forms.Label();
            this.reportingAuthor = new System.Windows.Forms.Label();
            this.resolveButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // bugName
            // 
            this.bugName.AutoSize = true;
            this.bugName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bugName.Location = new System.Drawing.Point(13, 13);
            this.bugName.Name = "bugName";
            this.bugName.Size = new System.Drawing.Size(41, 20);
            this.bugName.TabIndex = 0;
            this.bugName.Text = "Bug";
            // 
            // classNameLabel
            // 
            this.classNameLabel.AutoSize = true;
            this.classNameLabel.Location = new System.Drawing.Point(14, 46);
            this.classNameLabel.Name = "classNameLabel";
            this.classNameLabel.Size = new System.Drawing.Size(63, 13);
            this.classNameLabel.TabIndex = 1;
            this.classNameLabel.Text = "Class Name";
            // 
            // lineNumberLabel
            // 
            this.lineNumberLabel.AutoSize = true;
            this.lineNumberLabel.Location = new System.Drawing.Point(14, 72);
            this.lineNumberLabel.Name = "lineNumberLabel";
            this.lineNumberLabel.Size = new System.Drawing.Size(67, 13);
            this.lineNumberLabel.TabIndex = 2;
            this.lineNumberLabel.Text = "Line Number";
            // 
            // revisionNumberLabel
            // 
            this.revisionNumberLabel.AutoSize = true;
            this.revisionNumberLabel.Location = new System.Drawing.Point(14, 100);
            this.revisionNumberLabel.Name = "revisionNumberLabel";
            this.revisionNumberLabel.Size = new System.Drawing.Size(88, 13);
            this.revisionNumberLabel.TabIndex = 3;
            this.revisionNumberLabel.Text = "Revision Number";
            // 
            // codeAuthorLabel
            // 
            this.codeAuthorLabel.AutoSize = true;
            this.codeAuthorLabel.Location = new System.Drawing.Point(14, 127);
            this.codeAuthorLabel.Name = "codeAuthorLabel";
            this.codeAuthorLabel.Size = new System.Drawing.Size(66, 13);
            this.codeAuthorLabel.TabIndex = 4;
            this.codeAuthorLabel.Text = "Code Author";
            // 
            // reportingAuthorLabel
            // 
            this.reportingAuthorLabel.AutoSize = true;
            this.reportingAuthorLabel.Location = new System.Drawing.Point(14, 152);
            this.reportingAuthorLabel.Name = "reportingAuthorLabel";
            this.reportingAuthorLabel.Size = new System.Drawing.Size(87, 13);
            this.reportingAuthorLabel.TabIndex = 5;
            this.reportingAuthorLabel.Text = "Reporting Author";
            // 
            // className
            // 
            this.className.AutoSize = true;
            this.className.Location = new System.Drawing.Point(154, 46);
            this.className.Name = "className";
            this.className.Size = new System.Drawing.Size(10, 13);
            this.className.TabIndex = 6;
            this.className.Text = "-";
            // 
            // lineNumber
            // 
            this.lineNumber.AutoSize = true;
            this.lineNumber.Location = new System.Drawing.Point(154, 72);
            this.lineNumber.Name = "lineNumber";
            this.lineNumber.Size = new System.Drawing.Size(10, 13);
            this.lineNumber.TabIndex = 7;
            this.lineNumber.Text = "-";
            // 
            // revisionNumber
            // 
            this.revisionNumber.AutoSize = true;
            this.revisionNumber.Location = new System.Drawing.Point(154, 100);
            this.revisionNumber.Name = "revisionNumber";
            this.revisionNumber.Size = new System.Drawing.Size(10, 13);
            this.revisionNumber.TabIndex = 8;
            this.revisionNumber.Text = "-";
            // 
            // codeAuthor
            // 
            this.codeAuthor.AutoSize = true;
            this.codeAuthor.Location = new System.Drawing.Point(154, 127);
            this.codeAuthor.Name = "codeAuthor";
            this.codeAuthor.Size = new System.Drawing.Size(10, 13);
            this.codeAuthor.TabIndex = 9;
            this.codeAuthor.Text = "-";
            // 
            // reportingAuthor
            // 
            this.reportingAuthor.AutoSize = true;
            this.reportingAuthor.Location = new System.Drawing.Point(154, 152);
            this.reportingAuthor.Name = "reportingAuthor";
            this.reportingAuthor.Size = new System.Drawing.Size(10, 13);
            this.reportingAuthor.TabIndex = 10;
            this.reportingAuthor.Text = "-";
            // 
            // resolveButton
            // 
            this.resolveButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.resolveButton.Location = new System.Drawing.Point(157, 178);
            this.resolveButton.Name = "resolveButton";
            this.resolveButton.Size = new System.Drawing.Size(91, 51);
            this.resolveButton.TabIndex = 11;
            this.resolveButton.Text = "Mark as Resolved";
            this.resolveButton.UseVisualStyleBackColor = true;
            this.resolveButton.Click += new System.EventHandler(this.resolveButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelButton.Location = new System.Drawing.Point(17, 178);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(85, 51);
            this.cancelButton.TabIndex = 12;
            this.cancelButton.Text = "Close";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // BugView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(277, 289);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.resolveButton);
            this.Controls.Add(this.reportingAuthor);
            this.Controls.Add(this.codeAuthor);
            this.Controls.Add(this.revisionNumber);
            this.Controls.Add(this.lineNumber);
            this.Controls.Add(this.className);
            this.Controls.Add(this.reportingAuthorLabel);
            this.Controls.Add(this.codeAuthorLabel);
            this.Controls.Add(this.revisionNumberLabel);
            this.Controls.Add(this.lineNumberLabel);
            this.Controls.Add(this.classNameLabel);
            this.Controls.Add(this.bugName);
            this.Name = "BugView";
            this.Text = "BugView";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label bugName;
        private System.Windows.Forms.Label classNameLabel;
        private System.Windows.Forms.Label lineNumberLabel;
        private System.Windows.Forms.Label revisionNumberLabel;
        private System.Windows.Forms.Label codeAuthorLabel;
        private System.Windows.Forms.Label reportingAuthorLabel;
        private System.Windows.Forms.Label className;
        private System.Windows.Forms.Label lineNumber;
        private System.Windows.Forms.Label revisionNumber;
        private System.Windows.Forms.Label codeAuthor;
        private System.Windows.Forms.Label reportingAuthor;
        private System.Windows.Forms.Button resolveButton;
        private System.Windows.Forms.Button cancelButton;
    }
}