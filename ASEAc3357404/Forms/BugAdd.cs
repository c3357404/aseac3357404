﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlServerCe;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ASEAc3357404
{
    public partial class BugAdd : Form
    {
        private List _listView;
        SqlCeConnection connection;

        public BugAdd(List listView, string id, string name)
        {
            _listView = listView;
            InitializeComponent();
            connection = new SqlCeConnection(@"Data Source=I:\ASEAc3357404\ASEAdb.sdf");
            connection.Open();
            projectId.Text = id;
            projectName.Text = name;
        }

        private void createButton_Click(object sender, EventArgs e)
        {
            new Methods(connection).CreateNewBug(projectId.Text, bugName.Text, className.Text, lineNumber.Value, revisionNumber.Text, codeAuthor.Text, reportingAuthor.Text);
            _listView.populateBugs();

            this.Close();
        }
    }
}
