﻿namespace ASEAc3357404
{
    partial class BugAdd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.className = new System.Windows.Forms.TextBox();
            this.classNameLabel = new System.Windows.Forms.Label();
            this.lineNumberLabel = new System.Windows.Forms.Label();
            this.revisionNumber = new System.Windows.Forms.TextBox();
            this.revisionNumberLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.reportingAuthor = new System.Windows.Forms.TextBox();
            this.lineNumber = new System.Windows.Forms.NumericUpDown();
            this.Title = new System.Windows.Forms.Label();
            this.projectName = new System.Windows.Forms.Label();
            this.projectNameLabel = new System.Windows.Forms.Label();
            this.projectIdLabel = new System.Windows.Forms.Label();
            this.projectId = new System.Windows.Forms.Label();
            this.createButton = new System.Windows.Forms.Button();
            this.bugNameLabel = new System.Windows.Forms.Label();
            this.bugName = new System.Windows.Forms.TextBox();
            this.codeAuthorLabel = new System.Windows.Forms.Label();
            this.codeAuthor = new System.Windows.Forms.TextBox();
            this.codeLabel = new System.Windows.Forms.Label();
            this.code = new System.Windows.Forms.RichTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.lineNumber)).BeginInit();
            this.SuspendLayout();
            // 
            // className
            // 
            this.className.Location = new System.Drawing.Point(123, 144);
            this.className.Name = "className";
            this.className.Size = new System.Drawing.Size(150, 20);
            this.className.TabIndex = 1;
            // 
            // classNameLabel
            // 
            this.classNameLabel.AutoSize = true;
            this.classNameLabel.Location = new System.Drawing.Point(19, 147);
            this.classNameLabel.Name = "classNameLabel";
            this.classNameLabel.Size = new System.Drawing.Size(63, 13);
            this.classNameLabel.TabIndex = 1;
            this.classNameLabel.Text = "Class Name";
            // 
            // lineNumberLabel
            // 
            this.lineNumberLabel.AutoSize = true;
            this.lineNumberLabel.Location = new System.Drawing.Point(19, 179);
            this.lineNumberLabel.Name = "lineNumberLabel";
            this.lineNumberLabel.Size = new System.Drawing.Size(67, 13);
            this.lineNumberLabel.TabIndex = 2;
            this.lineNumberLabel.Text = "Line Number";
            // 
            // revisionNumber
            // 
            this.revisionNumber.Location = new System.Drawing.Point(123, 206);
            this.revisionNumber.Name = "revisionNumber";
            this.revisionNumber.Size = new System.Drawing.Size(150, 20);
            this.revisionNumber.TabIndex = 3;
            // 
            // revisionNumberLabel
            // 
            this.revisionNumberLabel.AutoSize = true;
            this.revisionNumberLabel.Location = new System.Drawing.Point(19, 209);
            this.revisionNumberLabel.Name = "revisionNumberLabel";
            this.revisionNumberLabel.Size = new System.Drawing.Size(88, 13);
            this.revisionNumberLabel.TabIndex = 5;
            this.revisionNumberLabel.Text = "Revision Number";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 270);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Reporting Author";
            // 
            // reportingAuthor
            // 
            this.reportingAuthor.Location = new System.Drawing.Point(123, 267);
            this.reportingAuthor.Name = "reportingAuthor";
            this.reportingAuthor.Size = new System.Drawing.Size(150, 20);
            this.reportingAuthor.TabIndex = 5;
            // 
            // lineNumber
            // 
            this.lineNumber.Location = new System.Drawing.Point(123, 177);
            this.lineNumber.Name = "lineNumber";
            this.lineNumber.Size = new System.Drawing.Size(150, 20);
            this.lineNumber.TabIndex = 2;
            // 
            // Title
            // 
            this.Title.AutoSize = true;
            this.Title.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Title.Location = new System.Drawing.Point(18, 9);
            this.Title.Name = "Title";
            this.Title.Size = new System.Drawing.Size(90, 22);
            this.Title.TabIndex = 9;
            this.Title.Text = "New Bug";
            // 
            // projectName
            // 
            this.projectName.AutoSize = true;
            this.projectName.Location = new System.Drawing.Point(120, 90);
            this.projectName.Name = "projectName";
            this.projectName.Size = new System.Drawing.Size(71, 13);
            this.projectName.TabIndex = 10;
            this.projectName.Text = "Project Name";
            // 
            // projectNameLabel
            // 
            this.projectNameLabel.AutoSize = true;
            this.projectNameLabel.Location = new System.Drawing.Point(19, 90);
            this.projectNameLabel.Name = "projectNameLabel";
            this.projectNameLabel.Size = new System.Drawing.Size(71, 13);
            this.projectNameLabel.TabIndex = 11;
            this.projectNameLabel.Text = "Project Name";
            // 
            // projectIdLabel
            // 
            this.projectIdLabel.AutoSize = true;
            this.projectIdLabel.Location = new System.Drawing.Point(21, 58);
            this.projectIdLabel.Name = "projectIdLabel";
            this.projectIdLabel.Size = new System.Drawing.Size(52, 13);
            this.projectIdLabel.TabIndex = 12;
            this.projectIdLabel.Text = "Project Id";
            // 
            // projectId
            // 
            this.projectId.AutoSize = true;
            this.projectId.Location = new System.Drawing.Point(119, 58);
            this.projectId.Name = "projectId";
            this.projectId.Size = new System.Drawing.Size(0, 13);
            this.projectId.TabIndex = 13;
            // 
            // createButton
            // 
            this.createButton.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.createButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.createButton.Location = new System.Drawing.Point(174, 512);
            this.createButton.Name = "createButton";
            this.createButton.Size = new System.Drawing.Size(99, 44);
            this.createButton.TabIndex = 6;
            this.createButton.Text = "Create Bug";
            this.createButton.UseVisualStyleBackColor = false;
            this.createButton.Click += new System.EventHandler(this.createButton_Click);
            // 
            // bugNameLabel
            // 
            this.bugNameLabel.AutoSize = true;
            this.bugNameLabel.Location = new System.Drawing.Point(19, 116);
            this.bugNameLabel.Name = "bugNameLabel";
            this.bugNameLabel.Size = new System.Drawing.Size(57, 13);
            this.bugNameLabel.TabIndex = 15;
            this.bugNameLabel.Text = "Bug Name";
            // 
            // bugName
            // 
            this.bugName.Location = new System.Drawing.Point(123, 113);
            this.bugName.Name = "bugName";
            this.bugName.Size = new System.Drawing.Size(150, 20);
            this.bugName.TabIndex = 0;
            // 
            // codeAuthorLabel
            // 
            this.codeAuthorLabel.AutoSize = true;
            this.codeAuthorLabel.Location = new System.Drawing.Point(21, 238);
            this.codeAuthorLabel.Name = "codeAuthorLabel";
            this.codeAuthorLabel.Size = new System.Drawing.Size(66, 13);
            this.codeAuthorLabel.TabIndex = 17;
            this.codeAuthorLabel.Text = "Code Author";
            // 
            // codeAuthor
            // 
            this.codeAuthor.Location = new System.Drawing.Point(122, 235);
            this.codeAuthor.Name = "codeAuthor";
            this.codeAuthor.Size = new System.Drawing.Size(150, 20);
            this.codeAuthor.TabIndex = 4;
            // 
            // codeLabel
            // 
            this.codeLabel.AutoSize = true;
            this.codeLabel.Location = new System.Drawing.Point(22, 297);
            this.codeLabel.Name = "codeLabel";
            this.codeLabel.Size = new System.Drawing.Size(32, 13);
            this.codeLabel.TabIndex = 18;
            this.codeLabel.Text = "Code";
            // 
            // code
            // 
            this.code.Location = new System.Drawing.Point(22, 314);
            this.code.Name = "code";
            this.code.Size = new System.Drawing.Size(251, 192);
            this.code.TabIndex = 19;
            this.code.Text = "";
            // 
            // BugAdd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(348, 637);
            this.Controls.Add(this.code);
            this.Controls.Add(this.codeLabel);
            this.Controls.Add(this.codeAuthor);
            this.Controls.Add(this.codeAuthorLabel);
            this.Controls.Add(this.bugName);
            this.Controls.Add(this.bugNameLabel);
            this.Controls.Add(this.createButton);
            this.Controls.Add(this.projectId);
            this.Controls.Add(this.projectIdLabel);
            this.Controls.Add(this.projectNameLabel);
            this.Controls.Add(this.projectName);
            this.Controls.Add(this.Title);
            this.Controls.Add(this.lineNumber);
            this.Controls.Add(this.reportingAuthor);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.revisionNumberLabel);
            this.Controls.Add(this.revisionNumber);
            this.Controls.Add(this.lineNumberLabel);
            this.Controls.Add(this.classNameLabel);
            this.Controls.Add(this.className);
            this.Name = "BugAdd";
            this.Text = "BugAdd";
            ((System.ComponentModel.ISupportInitialize)(this.lineNumber)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox className;
        private System.Windows.Forms.Label classNameLabel;
        private System.Windows.Forms.Label lineNumberLabel;
        private System.Windows.Forms.TextBox revisionNumber;
        private System.Windows.Forms.Label revisionNumberLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox reportingAuthor;
        private System.Windows.Forms.NumericUpDown lineNumber;
        private System.Windows.Forms.Label Title;
        private System.Windows.Forms.Label projectName;
        private System.Windows.Forms.Label projectNameLabel;
        private System.Windows.Forms.Label projectIdLabel;
        private System.Windows.Forms.Label projectId;
        private System.Windows.Forms.Button createButton;
        private System.Windows.Forms.Label bugNameLabel;
        private System.Windows.Forms.TextBox bugName;
        private System.Windows.Forms.Label codeAuthorLabel;
        private System.Windows.Forms.TextBox codeAuthor;
        private System.Windows.Forms.Label codeLabel;
        private System.Windows.Forms.RichTextBox code;
    }
}