﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe;

namespace ASEAc3357404
{
    public partial class ProjectAdd : Form
    {
        SqlCeConnection connection;
        List _listView;

        public ProjectAdd(List listView)
        {
            InitializeComponent();
            connection = new SqlCeConnection(@"Data Source=I:\ASEAc3357404\ASEAdb.sdf");
            connection.Open();
            _listView = listView;

        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            // Close the form
            this.Close();
        }

        private void createButton_Click(object sender, EventArgs e)
        {
            new Methods(connection).CreateNewProject(projectName.Text.ToString(), author.Text.ToString());

            _listView.populateProjects();

            // Close off the form, finally
            this.Close();
        }
    }
}
