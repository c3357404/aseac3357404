﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlServerCe;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ASEAc3357404
{
    public partial class BugView : Form
    {
        private List _listView;
        string _bugId;
        SqlCeConnection connection;

        public BugView(List listView, string bugId)
        {
            _listView = listView;
            InitializeComponent();
            connection = new SqlCeConnection(@"Data Source=I:\ASEAc3357404\ASEAdb.sdf");
            connection.Open();
            _bugId = bugId;
            populateBugView();
        }

        private void populateBugView()
        {
            var bugDto = new Methods(connection).GetBug(_bugId);

            bugName.Text = bugDto.BugName;
            className.Text = bugDto.ClassName;
            lineNumber.Text = bugDto.LineNumber.ToString();
            revisionNumber.Text = bugDto.RevisionNumber;
            codeAuthor.Text = bugDto.CodeAuthor;
            reportingAuthor.Text = bugDto.ReportingAuthor;
        }

        private void resolveButton_Click(object sender, EventArgs e)
        {
            new Methods(connection).MarkBugAsResolved(_bugId);
            _listView.populateBugs();

            this.Close();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
