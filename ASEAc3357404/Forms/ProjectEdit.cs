﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlServerCe;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ASEAc3357404
{
    public partial class ProjectEdit : Form
    {
        private List _listView;
        string projectId;
        SqlCeConnection connection;

        public ProjectEdit(List listView, string id)
        {
            _listView = listView;
            InitializeComponent();
            projectId = id;
            connection = new SqlCeConnection(@"Data Source=I:\ASEAc3357404\ASEAdb.sdf");
            connection.Open();
            populateFields(projectId);
        }

        private void populateFields(string projectId)
        {
            var project = new Methods(connection).GetProject(projectId);

            projectName.Text = project.ProjectName;
            author.Text = project.Author;
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            new Methods(connection).EditProject(projectId, projectName.Text, author.Text);
            _listView.populateProjects();
            this.Close();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
