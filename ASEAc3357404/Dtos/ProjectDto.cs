﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASEAc3357404.Dtos
{
    public class ProjectDto
    {
        public int ProjectId { get; set; }

        public string ProjectName { get; set; }

        public string Author { get; set; }
    }
}
