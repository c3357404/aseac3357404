﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASEAc3357404.Dtos
{
    public class BugDto
    {
        public int BugId { get; set; }

        public int ProjectId { get; set; }

        public string BugName { get; set; }

        public string ClassName { get; set; }

        public int LineNumber { get; set; }

        public string RevisionNumber { get; set; }

        public string CodeAuthor { get; set; }

        public string ReportingAuthor { get; set; }
    }
}
